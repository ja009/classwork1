package fourthpackage;


import java.util.HashMap;

public class HashMapPractice {
    public static void main(String[] args) {

        //Key value pair
        HashMap<String,Integer> mapOfCountries = new HashMap();
        mapOfCountries.put("NY",11416);
        mapOfCountries.put("NJ",11134);
        mapOfCountries.put("CA",33320);
        mapOfCountries.put("MI",22283);

        System.out.println(mapOfCountries.get("CA"));
        System.out.println(mapOfCountries.size());


        HashMap<String,String> mapOfAmerica = new HashMap();
        mapOfAmerica.put("NY","New York");
        mapOfAmerica.put("NJ","New Jersey");
        mapOfAmerica.put("CA","California");
        mapOfAmerica.put("FL","Florida");

        System.out.println(mapOfAmerica.size());
        System.out.println(mapOfAmerica);


    }

}
