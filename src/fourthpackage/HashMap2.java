package fourthpackage;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMap2 {
    public static void main(String[] args) {
        ArrayList<String> listOfCapitals = new ArrayList<>();
        listOfCapitals.add("Japan");
        listOfCapitals.add("England");
        listOfCapitals.add("France");
        listOfCapitals.add("Finland");


        HashMap<String, ArrayList<String>> newMap = new HashMap<>();
        newMap.put("Countries",listOfCapitals);
        System.out.println(newMap.get("Countries").get(2));

    }//Create Array List of hash map, store data in it and see how it goes for homework.
}
