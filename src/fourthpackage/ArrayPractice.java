package fourthpackage;

public class ArrayPractice {
    public static void main(String[] args) {
        /*String name1 = "Md";
        String name2 = "Sub";
        String name3 = "JAwad";


        System.out.println(name1);
        System.out.println(name2);
        System.out.println(name3);*/

        String [] names = {"Md","Jawad","Sub","Zan"};

        System.out.println(names.length);//this gives the length of the array

        for (int i = 0;i< names.length;i++){//if you store the length of the array inside the loop, it will automatically update based on how many entires/values there are inside the loop.
            System.out.println(names[i]);
        }

        System.out.println("******************************");

        String [] states = {"New York","New Jersey","Connecticut","California","Texas"};

        for (int i = 0; i < states.length; i++){
            if (states[i].equals("New York")||states[i].equals("New Jersey")||states[i].equals("Connecticut")){
                System.out.println("Tristate.");
            }else {
                System.out.println("other");
            }
        }


    }
}
