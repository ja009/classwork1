package fourthpackage;

import java.util.ArrayList;

public class ArrayListPractice {
    public static void main(String[] args) {
        ArrayList<String> listOfNames = new ArrayList<>();
        listOfNames.add("Md");
        listOfNames.add("Jawad");
        listOfNames.add("SUb");
        listOfNames.add("Zann");

        for(int i=0;i<listOfNames.size();i++){
            System.out.println(listOfNames.get(i));
        }

    }
}
