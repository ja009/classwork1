package secondpackage;

// create 2 static variables and 1 non static variables (with value)
// create 1 static method and 1 non static method

// print the static variables into non static method
// print the static variables into static method

// print the non static variables into non static method
// print the non static variable into static method

// finally call both the methods in the main method 

public class AccessSpecifier {
    public static String name = "Jawad";
    public static String age = "25";// create 2 static variables and 1 non static variables (with value)
    public static AccessSpecifier ObjectName = new AccessSpecifier();
    String year = "2022";//1 non static variables (with value)

    public static void personalinfo() {// create 1 static method
        System.out.println(name);// print the static variables into static method
        System.out.println(age);// print the static variables into static method
        System.out.println(ObjectName.year);// print the non static variable into static method
    }

    public static void main(String[] args) {
        personalinfo();
        ObjectName.sensitiveinfo();

    }

    public void sensitiveinfo() {// create 1 non static method
        System.out.println(name);// print the static variables into non static method
        System.out.println(age);// print the static variables into non static method
        System.out.println(year);// print the non static variables into non static method
    }

}

	
	

	
