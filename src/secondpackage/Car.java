package secondpackage;

public class Car {

    private final String name;
    private final String car;
    private final String model;
    private final int year;
    public Car(String name, String car, String model, int year) {
        this.name = name;
        this.car = car;
        this.model = model;
        this.year = year;
    }

    public void privatelyOwned() {
        System.out.println(name);
        System.out.println(car);
        System.out.println(model);
        System.out.println(year);


    }

    public void taxiDrivers() {
        System.out.println(name);
        System.out.println(car);
        System.out.println(model);
        System.out.println(year);

    }
}
