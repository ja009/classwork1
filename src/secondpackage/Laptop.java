package secondpackage;

public class Laptop {

    private static final String model = "Macbook Air";
    private final String make = "Apple";

    public static void printLaptopModel() {
        System.out.println(model);

    }

    public void printLaptopMake() {
        System.out.println(make);
    }


}
