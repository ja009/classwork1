package secondpackage;

public class LaptopExecution {


    public static void main(String[] args) {

        //Static method, in order to go to different class, you have to call via class
        Laptop.printLaptopModel();

        Laptop object = new Laptop();
        object.printLaptopMake();

        AccessSpecifier.personalinfo();

        AccessSpecifier Object2 = new AccessSpecifier();
        Object2.sensitiveinfo();

    }


}
