package thirdpackage;

public class SoccerTeam {

    static String nameOfTheClub;
    private String nameOfTrainer;

    private String manager;
    private int totalPlayers;


    public SoccerTeam(String manager, int totalPlayers) {
        this.manager = manager;
        this.totalPlayers = totalPlayers;
    }

    public void league1(){
        System.out.println(manager + " is managing the team this season.");
        System.out.println(totalPlayers + " is the number of the players.");
    }

    public static String getNameOfTheClub(){
        return nameOfTheClub + " is the name of the club.";
    }

    public String getNameOfTrainer(){
        return nameOfTrainer + " is the trainer of the club";
    }

    public void setNameOfTrainer(String name){
        this.nameOfTrainer = name;
    }
}
