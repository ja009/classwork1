package thirdpackage;

public class SoccerTeamExecution {
    public static void main(String[] arg){

        //setup the required so that league 1 prints details
        SoccerTeam team = new SoccerTeam("Jawad",23);
        team.league1();

        //setup the required and get the value of getNameOfTheClub and print
        SoccerTeam.nameOfTheClub="Arsenal";
        String name = SoccerTeam.getNameOfTheClub();
        System.out.println(name);


        //
        team.setNameOfTrainer("Sub");
        String nameOfTrainerOfSoccerTeam = team.getNameOfTrainer();
        System.out.println(nameOfTrainerOfSoccerTeam);

    }
}
